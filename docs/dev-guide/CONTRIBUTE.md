# Comment contribuer à Framadate funky ?
Il existe des tas de façons de contribuer à un logiciel libre comme Framadate. Vous pouvez __discuter__ avec d'autres personnes de ce que vous souhaiteriez voir naître dans le logiciel, __essayer__ de l'utiliser dans sa version expérimentale, __mettre en place une démo__ ou un service publiquement utilisable, __écrire__ des modifications de code, proposer de l'aide de toute sorte, [traduire les textes](../cadrage/i18n.md), vérifier l'accessibilité, lire la [documentation d'architecture](../cadrage/architecture.md) etc... 

* Avoir un compte Framateam, et Framagit si vous souhaitez contribuer au code
* Examiner les tickets 
Discuter avec les autres membres participant, sur un ticket en particulier et aussi dans les canaux de framateam
* Nous avons créé deux canaux: un pour les discussions générales, et un autre pour les discussions techniques de dev. Prenez soin de bien cibler le canal dans lequel vous communiquez afin de ne pas faire peur aux gens qui ne codent pas et qui souhaitent contribuer de toutes les autres façons.
* Une fois d'accord avec les autres, mettre à jour votre dépot de travail local. Voir [la doc d'installation / getting started](../GETTING_STARTED.md) à ce sujet.
* Utiliser la branche `develop`
    ```
    git checkout develop
    git fetch
    ```
* choisir un ticket gitlab consacré à votre branche, si il n'en existe pas, le créer. Un ticket doit avoir un sujet suffisament petit pour pouvoir le réaliser dans un temps raisonnable. Faites des sous-tickets reliés pour les longues fonctionnalités. Le but des tickets n'étant pas de rester ad vitam dans la backlog, mais bien de montrer un avancement détaillé, avec un titre évocateur. "exit donc les tickets du genre 'ça ne marche pas' ou 'finir framadate'"
* Faire une branche dédiée à vos modifications en lui mettant un nom éloquent.
    ```
    git checkout -b ma-description-de-fonctionnalite
    ```
* lisez les logs des commits les plus récents pour comprendre ce qui se passe.
* Faites des petits commits, avec un titre désignant précisément ce sur quoi vous progressez.
* Faire une merge request sur framagit qui sera soumise à la revue de code par les pairs du projet.
* Continuer à interagir avec les autres membres pour utiliser au mieux les savoir-faire de chacun et ne pas se marcher sur les pieds. 
* Mettez toujours en avant la politesse et l'empathie, la collaboration n'en sera que meilleure.
* N'hésitez pas à contacter en direct les personnes avec lesquelles vous souhaitez avancer.

# Qui veut faire quoi ?

* maiwann : maquettes, UX
* tykayn : développeur front end, JS & styles
* newick : intégrateur dans la vraie vie
* llaq : plutôt HTML / css et un peu de développement en php
* talone : plutôt coté JS
* tcit : dev tout qui connait bien le backend de Framadate actuel
* pouhiou : soutien moral
* come_744 : git, découverte d'angular
* arnaldo : php, découverte du libre
* elbuffeto :  l'intégration HTML/CSS, accessibilité
* l4pin : un peu de front JS, beaucoup de back
* wadouk : dev compilé (elm, haskell, scala)
* cbossard : dev (plutôt backend), java/javascript, avec un peu de temps en ce moment
* seraf : dev fullstack (plutôt front), JS (Angular, VueJS, Svelte), PHP (Symfony, ApiPlatform), Java (Spring)


# Liens utiles:
* Discussion : https://framateam.org/ux-framatrucs/channels/framadate
* Repo front/dev : https://framagit.org/framasoft/framadate/funky-framadate-front/tree/dev 
* Repo back : https://framagit.org/framasoft/framadate/framadate 
* Maquettes Zeplin : demander l'accès à maiwann
* La démo : https://framadate-api.cipherbliss.com/
* Vidéo de présentation au lancement de la refonte : https://nuage.maiwann.net/s/JRRHTR9D2akMAa7

