import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ConsultationComponent } from './consultation.component';
import { WipTodoComponent } from '../../shared/components/ui/wip-todo/wip-todo.component';
import { PasswordPromptComponent } from './password/password-prompt/password-prompt.component';
import { ConsultationLandingComponent } from './consultation-landing/consultation-landing.component';
import { SuccessComponent } from './success/success.component';
import { ConsultationUserComponent } from './consultation-user/consultation-user.component';
import { PageNotFoundComponent } from '../../shared/components/page-not-found/page-not-found.component';
import { EditComponent } from './edit/edit.component';

const routes: Routes = [
	{
		path: '',
		component: ConsultationComponent,
	},
	{ path: 'simple', component: ConsultationComponent },
	{ path: 'secure/:pass_hash', component: ConsultationComponent },
	{ path: 'prompt', component: PasswordPromptComponent },
	{ path: 'table', component: WipTodoComponent },
	{ path: 'vote', component: EditComponent },
	{ path: 'vote/user-infos', component: ConsultationUserComponent },
	{ path: 'edit/:voteStackId', component: EditComponent },
	{ path: 'success', component: SuccessComponent },
	{ path: 'page-not-found', component: PageNotFoundComponent },
	{ path: '**', redirectTo: 'page-not-found', pathMatch: 'full' },
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
})
export class ConsultationRoutingModule {}
