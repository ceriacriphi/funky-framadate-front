import { Component, Input, OnInit } from '@angular/core';
import { MatDialogConfig } from '@angular/material/dialog';
import { Answer } from 'src/app/core/enums/answer.enum';

import { Choice } from '../../../core/models/choice.model';
import { Poll } from '../../../core/models/poll.model';
import { Owner } from '../../../core/models/owner.model';
import { ModalService } from '../../../core/services/modal.service';
import { PollService } from '../../../core/services/poll.service';
import { ChoiceDetailsComponent } from '../../../shared/components/choice-details/choice-details.component';

@Component({
	selector: 'app-add-answer',
	templateUrl: './add-answer.component.html',
	styleUrls: ['./add-answer.component.scss'],
})
export class AddAnswerComponent {
	@Input() user: Owner;
	@Input() poll: Poll;
	@Input() choice: Choice;
	public answerEnum = Answer;
	public answer: any;

	constructor(private pollService: PollService, private modalService: ModalService) {}

	public openModal(choice: Choice): void {
		const config: MatDialogConfig<Choice> = { data: choice };
		this.modalService.openModal<ChoiceDetailsComponent, Choice>(ChoiceDetailsComponent, config);
	}

	public vote(answer: string): void {
		this.answer = answer as Answer;
		this.pollService.saveParticipation(this.choice, this.user, this.answer);
	}
}
